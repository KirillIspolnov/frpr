<!doctype>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
	</head>
	<body>
		<?php
			if(isset($_POST["rows"]) && isset($_POST["cols"])):
				$rows = $_POST["rows"];
				$cols = $_POST["cols"];

				$matrix = array();
				$numbers = array();

				for($i = 0; $i < $rows; $i++) {
					$matrix[] = array();
					for($j = 0; $j < $cols; $j++) {
						$matrix[$i][$j] = rand(-10000, 15000);
					}
				}

				$summ = 0;
				$min = PHP_INT_MAX;
				$max = PHP_INT_MIN;
				$lastMax;
				foreach($matrix as $row) {
					foreach ($row as $value) {
						$summ += $value;

						if($value < $min) {
							$min = $value;
						}
						if($value > $max) {
							$lastMax = $max;
							$max = $value;
						}

						if($value % 2 == 0 && $value > 300 && $value < 1000) {
							$numbers[] = $value;
						}
 					}
				}

				echo $min.":".$max;

				$avS = $summ / ($cols * $rows);
				$avH = ($min + $max) / 2;

 		?>
 		<h3>Готов двухмерный массив:</h3>
 		<table>
 		<?php
 			echo "<tr><th></th>";
 			for($i = 0; $i < $cols; $i++) {
 				echo "<th>$i</th>";
 			}
 			echo "</tr>";

 			foreach($matrix as $key => $row) {
 				echo "<tr><th>$key</th>";
 				foreach($row as $value) {
 					echo "<td>$value</td>";
 				}
 				echo "</tr>";
 			}
 		?>
 		</table>
 		<h3>Готовы необходимые вычисления:</h3>
 		<?php
 			echo "Сумма всех ".($cols*$rows)." элементов $summ<br />";
 			echo "Среднее значение элементов (простое): $avS<br />";
 			echo "Среднее значение элементов (сложное): $avH<br />";
 			echo "Второй по величине максимум: $lastMax<br />";
 			echo "Количество четных чисел, величина которых разнится от 300 до 1000: ".count($numbers)."<br />";
 			echo "Кореляция средних значений (отношение, разность): ".($avS/$avH).", ".($avS-$avH)."<br />";
 		?>
		<?php 
			else:
		?>
		<form action="/30.php" method="post">
			<label>Кол-во строк</label>
			<input type="number" name="rows"/><br />
			<label>Кол-во столбцов</label>
			<input type="number" name="cols"/><br />
			<input type="submit"/>
		</form>
		<?php
			endif;
		?>
	</body>
</html>