<!doctype>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<style>
			BODY {
				font-size: 150%;
			}
		</style>
	</head>
	<body>
		<?php
			//проверка наличия данных из формы
			if(isset($_POST["rows"]) && isset($_POST["cols"])):
				$rows = $_POST["rows"];
				$cols = $_POST["cols"];

				//двумерный массив
				$matrix = array();
				//числа отобранные по комлпексному признаку
				$numbers = array();

				//генерация
				for($i = 0; $i < $rows; $i++) {
					//добавляем новую строку матрицы
					$matrix[] = array();
					//заполняем строку матрицы
					for($j = 0; $j < $cols; $j++) {
						//случайными числами от -10000 до 15000
						$matrix[$i][$j] = rand(-10000, 15000);
					}
				}

				//сумма
				$summ = 0;
				//минимум
				$min = PHP_INT_MAX;
				//максимум
				$max = PHP_INT_MIN;
				//второй максимум
				$lastMax;
				//перебор строк матрицы
				foreach($matrix as $row) {
					//перебор чисел в строке
					foreach ($row as $value) {
						$summ += $value;

						//поиск минимума
						if($value < $min) {
							$min = $value;
						}
						//поиск максимума
						if($value > $max) {
							//сохраняем предыдущий максимум
							//так можно нва два самых больших в массиве числа
							$lastMax = $max;
							$max = $value;
						}

						//комплексное условие
						if($value % 2 == 0 && $value > 300 && $value < 1000) {
							//сохраняем найденное число
							$numbers[] = $value;
						}
 					}
				}

				//обычное среднее значение
				$avS = $summ / ($cols * $rows);
				//среднее значение для больших массивов
				$avH = ($min + $max) / 2;
 		?>
 		<!-- этот HTML код будет отображен только если вы уже заполнили форму и нажали кнопку отправить -->
 		<h3>Готов двухмерный массив:</h3>
 		<table>
 		<?php
 			//формируем таблицу
 			//первая строка - заголовки - номера элементов в строках
 			echo "<tr><th></th>";
 			for($i = 0; $i < $cols; $i++) {
 				echo "<th>$i</th>";
 			}
 			echo "</tr>";

 			//вывод значений матрицы
 			foreach($matrix as $key => $row) {
 				//вывод номера строки
 				echo "<tr><th>$key</th>";
 				//вывод чисел из строки
 				foreach($row as $value) {
 					echo "<td>$value</td>";
 				}
 				echo "</tr>";
 			}
 		?>
 		</table>
 		<h3>Готовы необходимые вычисления:</h3>
 		<?php
 			echo "Сумма всех ".($cols*$rows)." элементов $summ<br />";
 			echo "Среднее значение элементов (простое): $avS<br />";
 			echo "Среднее значение элементов (сложное): $avH<br />";
 			echo "Второй по величине максимум: $lastMax<br />";
 			echo "Количество четных чисел, величина которых разнится от 300 до 1000: ".count($numbers)."<br />";
 			echo "Кореляция средних значений (отношение, разность): ".($avS/$avH).", ".($avS-$avH)."<br />";
 		?>
		<?php 
			else:
		?>
		<!-- форма -->
		<form action="" method="post">
			<label>Кол-во строк</label>
			<input type="number" name="rows"/><br />
			<label>Кол-во столбцов</label>
			<input type="number" name="cols"/><br />
			<input type="submit"/>
		</form>
		<?php
			endif;
		?>
	</body>
</html>