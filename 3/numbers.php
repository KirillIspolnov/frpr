<?php
	$numbers = explode(" ", file_get_contents("nums.txt"));
	
	$frequency = array();
	foreach($numbers as $num) {
		$frequency[$num]++;
	}
	
	arsort($frequency);
	
	echo "<table>";
	foreach($frequency as $key => $num) {
		echo "<tr><th>$key</th><td>$num</td></tr>";
	}
	echo "</table>"
	
?>