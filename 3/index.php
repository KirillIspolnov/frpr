<!doctype>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<style>
			BODY {
				font-size: 150%;
			}
		</style>
	</head>
	<body>
		<?php
			define("FILE", "text.txt");
			$text = file_get_contents(FILE);
		
			if(isset($_GET["mode"])) {
				$mode = $_GET["mode"];
				
				if($mode == "std") {
					$count = preg_match("/(\.\s*\w)/u", $text, $matches);
					foreach($matches as $m) {
						$text = str_replace($m, mb_convert_case($m, MB_CASE_UPPER, "UTF-8"), $text);
					}
					$text1 = substr($text, 1);
					$text2 = mb_convert_case($text, MB_CASE_UPPER, "UTF-8");
					$text = substr($text2, 0, 1).$text1;
				} else {
					$mode1;
					switch($mode) {
						case "upper": $mode1 = MB_CASE_UPPER; break;
						case "lower": $mode1 = MB_CASE_LOWER; break;
						case "title": $mode1 = MB_CASE_TITLE; break; 
					}
					$text = mb_convert_case($text, $mode1, "UTF-8");
				}
			}
			
			//файл с числами
			//отсортировать по возрастанию частоты каждого числа
			
			echo $text;
		?>
 		<form action="" method="get">
 			<select name="mode">
 				<option value="lower" <?php if($mode == "lower"){ echo "selected"; } ?>>Нижний регистр</option>
 				<option value="upper" <?php if($mode == "upper"){ echo "selected"; } ?>>Верхний регистр</option>
 				<option value="title" <?php if($mode == "title"){ echo "selected"; } ?>>Каждое слово с заглавной буквы</option>
 				<option value="std"   <?php if($mode == "std"){ echo "selected"; } ?>>Предложения с заглавной буквы</option>
 			</select>
 			<input type="submit"/>
 		</form>
	</body>
</html>