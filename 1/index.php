<!doctype>
<html>
	<head>
		<meta charset="utf-8"/>
		<title></title>
		<style>
			BODY {
				font-size: 150%;
			}
		</style>
	</head>
	<body>
		<?php
			//проверка наличия данных из формы
			if(isset($_POST["rows"]) && isset($_POST["cols"])):
				$rows = $_POST["rows"];
				$cols = $_POST["cols"];

				//двумерный массив
				$matrix = array();
				//числа отобранные по комлпексному признаку
				$numbers = array();

				//генерация
				for($i = 0; $i < $rows; $i++) {
					//добавляем новую строку матрицы
					$matrix[] = array();
					//заполняем строку матрицы
					for($j = 0; $j < $cols; $j++) {
						//случайными числами от -10000 до 15000
						//$matrix[$i][$j] = rand(-10000, 15000);
						$matrix[$i][$j] = "   ";
					}
				}

				function h($m, $x, $y, $matrix) {
					switch($m) {
						case 0: $x+=2; break;
						case 1: $y+=2; break;
						case 2: $x-=2; break;
						case 3: $y-=2; break;
					}
					
					if($x < 1 || $x > count($matrix)) {
						return true;
					}
					if($y < 1 || $y > count($matrix[0])) {
						return true;
					}
					
					return $matrix[$y][$x] == 1;
				}
				
				$x = $y = 0;
				$s = $cols;
				$m = 0;
				for($a = 0; $a < $cols; $a++) {
					for($i = 0; $i < $s; $i++) {
						switch($m) {
							case 0: $x++; break;
							case 1: $y++; break;
							case 2: $x--; break;
							case 3: $y--; break;
						}
						$matrix[$y][$x] = " * ";
					}
					
					$s--;
					
					if(++$m > 3) {
						$m = 0;
					}
				}
				
				function transpone($m) {
					global $cols;
					global $rows;
					
					$n = array();
					for($i = 0; $i < $rows; $i++) {
						$n[] = array();
					}
					
					for($i = 0; $i < $rows; $i++) {
						for($j = 0; $j < $cols; $j++) {
							$n[$j][$cols - 1 - $i] = $m[$i][$j];
						}
					}
					return $n;
				}
 		?>
 		<!-- этот HTML код будет отображен только если вы уже заполнили форму и нажали кнопку отправить -->
 		<h3>Готов двухмерный массив:</h3>
 		<table>
 		<?php
 			//формируем таблицу
 			//первая строка - заголовки - номера элементов в строках
 			 			//вывод значений матрицы
 			foreach($matrix as $key => $row) {
 				//вывод номера строки
 				echo "<tr>";
 				//вывод чисел из строки
 				foreach($row as $value) {
 					echo "<td>$value</td>";
 				}
 				echo "</tr>";
 			}
 		?>
 		</table>
 		<?php 
			else:
		?>
		<!-- форма -->
		<form action="" method="post">
			<label>Кол-во строк</label>
			<input type="number" name="rows"/><br />
			<label>Кол-во столбцов</label>
			<input type="number" name="cols"/><br />
			<input type="submit"/>
		</form>
		<?php
			endif;
		?>
	</body>
</html>